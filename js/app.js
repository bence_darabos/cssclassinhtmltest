var app = angular.module("toolApp", []);
 
app.controller("AppCtrl", ['$scope', '$http', function($scope, $http) {
	$scope.cssSource = "testsoruce.css";
	$scope.htmlSource = "testsource.html";
	$scope.getCss = function(filename){
		$http.get($scope.cssSource).success (function(data){
			$scope.filecontent = data;
			var filter = new RegExp("\\.(.*)\\s?{","g");
			var match;
			var taglist = [];
			var cleantaglist = [];
			while (match = filter.exec($scope.filecontent)){
				taglist.push(match[1]);
			}
			for(var i=0; i<taglist.length; i++){
				//TODO - comment handling
				//TODO - clear out every pseudo classes, selectors and dots.
				taglist[i] = taglist[i].replace(new RegExp('[ ]', 'g'),'');
				taglist[i] = taglist[i].replace(new RegExp('[,]', 'g'),'');
				taglist[i] = taglist[i].replace(new RegExp('[>]', 'g'),'');
				taglist[i] = taglist[i].replace(new RegExp('[:]', 'g'),'');
				taglist[i] = taglist[i].replace(new RegExp('hover', 'g'),'');
				taglist[i] = taglist[i].replace(new RegExp('first-line', 'g'),'');
			}
			for(var k=0; k<taglist.length; k++){
				var res = taglist[k].split(".");
				for(var j=0; j<res.length; j++){
					if(cleantaglist.indexOf(res[j])<0){

						cleantaglist.push(res[j]);
					}
				}
			}
			$scope.taglist = cleantaglist;
			console.log(taglist);
		});
	};
	$scope.getHtml = function(filename){
		$http.get($scope.htmlSource).success (function(data){
			$scope.filecontent = data;
			//var filter = new RegExp("class\\s?=\\s?[\"\'](.*)[\"\']","g");
			//var filter = new RegExp("(<\\w+?\\s+?class\\s*=\\s*['\"][^'\"]*?\\b)(.*)\\b","g");
			var filter = new RegExp("(class\\s*=\\s*['\"][^'\"]*?\\b)(.*)\\b\"","g");
			var match;
			var taglist = [];
			var cleantaglist = [];
			while (match = filter.exec($scope.filecontent)){
				console.log(match);
				taglist.push(match[2]);
			}
			for(var i=0; i<taglist.length; i++){
				//TODO - comment handling
				var res = taglist[i].split(" ");
				for(var j=0; j<res.length; j++){
					if(cleantaglist.indexOf(res[j])<0){
						cleantaglist.push(res[j]);
					}
				}
			}			
			$scope.taglist = cleantaglist;
			console.log(taglist);
			console.log(cleantaglist);
		});
	};
}]);